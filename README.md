# Code Challenge
 
## Getting the application
 
Clone the project at branch `master`.

## Running the application
mvn spring-boot:run
 
## Running the tests
mvn test
 