package com.lucidcache.service;

import com.lucidcache.model.Statistics;
import com.lucidcache.model.Transaction;

import java.time.Instant;

public interface TransactionService {
    void submitTransaction(Transaction t, long requestTimestamp);

    Statistics getStatistics(Instant instant);
}
