package com.lucidcache.service;

import com.lucidcache.model.Statistics;
import com.lucidcache.model.Transaction;
import com.lucidcache.util.TransactionBucket;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final int STAT_COMPRESSION_RATIO = 1000;

    @Value("${statistics.depth.in.seconds}")
    private int statisticsDepthInSeconds;

    private final List<TransactionBucket> transactionBuckets = new ArrayList<>(statisticsDepthInSeconds);

    private final Statistics emptyStatistics = new Statistics(0, 0, 0, 0);

    @PostConstruct
    private void init() {
        for (int i = 0; i < statisticsDepthInSeconds; i++) {
            transactionBuckets.add(new TransactionBucket());
        }
    }

    @Override
    public Statistics getStatistics(Instant requestInstant) {
        // Write operations to the collection may occur while gathering statistics
        // Although access to each bucket is threadsafe,
        // some buckets may be updated after the start of the collection of statistics but before they are accessed
        // Trade-off between the algorithm throughput and consistency/precision of statistics should be considered
        Statistics s = transactionBuckets.parallelStream()
                .map(t -> t.getStatistics(requestInstant, statisticsDepthInSeconds))
                .reduce((statistics1, statistics2) -> statistics1.add(statistics2))
                .orElse(emptyStatistics);
        return s.getCount() == 0 ? emptyStatistics : s;
    }

    @Override
    public void submitTransaction(Transaction transaction, long requestTimestamp) {
        int index = getTransactionBucketIndex(transaction);
        transactionBuckets.get(index).submitTransaction(transaction, STAT_COMPRESSION_RATIO);
    }

    private int getTransactionBucketIndex(Transaction transaction) {
        return (int) (transaction.getTimestamp() / STAT_COMPRESSION_RATIO % statisticsDepthInSeconds);
    }
}
