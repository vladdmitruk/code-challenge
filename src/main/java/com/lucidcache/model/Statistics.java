package com.lucidcache.model;

public class Statistics {
    private final double sum;
    private final double max;
    private final double min;
    private final long count;

    public Statistics(double sum, double max, double min, long count) {
        this.sum = sum;
        this.max = max;
        this.min = min;
        this.count = count;
    }

    public double getSum() {
        return sum;
    }

    public double getAvg() {
        return count != 0.0 ? sum / count : 0.0;
    }

    public double getMax() {
        return max;
    }

    public double getMin() {
        return min;
    }

    public long getCount() {
        return count;
    }

    public Statistics add(Statistics other) {
        return new Statistics(
                this.sum + other.getSum(),
                Double.max(this.max, other.max),
                Double.min(this.min, other.min),
                this.count + other.count);
    }
}
