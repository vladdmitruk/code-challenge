package com.lucidcache.controller;

import com.lucidcache.model.Statistics;
import com.lucidcache.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;

@RestController
@RequestMapping("/api")
public class StatisticsController {

    @Autowired
    TransactionService transactionService;

    @RequestMapping(path = "/statistics", method = RequestMethod.GET)
    public Statistics getStatistics() {
        Instant requestInstant = Instant.now();
        return transactionService.getStatistics(requestInstant);
    }
}
