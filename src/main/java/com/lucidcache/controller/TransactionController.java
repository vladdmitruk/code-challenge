package com.lucidcache.controller;

import com.lucidcache.model.Transaction;
import com.lucidcache.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;

@RestController
@RequestMapping("/api")
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    @Value("${statistics.depth.in.seconds}")
    private int statisticsDepthInSeconds;

    @RequestMapping(path = "/transactions", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Object> postTransaction(@RequestBody Transaction transaction) {

        Instant requestInstant = Instant.now();
        Instant transactionInstant = Instant.ofEpochMilli(transaction.getTimestamp());

        if (!isTransactionValid(transaction)) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
        } else if (transactionInstant.isBefore(requestInstant.minusSeconds(statisticsDepthInSeconds))) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } else {
            transactionService.submitTransaction(transaction, requestInstant.toEpochMilli());
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
    }

    private boolean isTransactionValid(Transaction transaction) {
        return !(transaction.getTimestamp() < 0 || Double.compare(transaction.getAmount(), 0.0) < 0);
    }
}
