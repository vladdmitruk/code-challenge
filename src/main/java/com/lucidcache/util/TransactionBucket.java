package com.lucidcache.util;

import com.lucidcache.model.Statistics;
import com.lucidcache.model.Transaction;

import java.time.Instant;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class TransactionBucket {

    private final ReadWriteLock lock = new ReentrantReadWriteLock(true);

    private double sum = 0.0;
    private double max = Double.MIN_VALUE;
    private double min = Double.MAX_VALUE;
    private long count = 0;
    private long timestamp = 0;

    public TransactionBucket() {
    }

    public void submitTransaction(Transaction transaction, int compressionRatio) {
        long compressedTimestamp = transaction.getTimestamp() / (compressionRatio == 0 ? 1 : compressionRatio);
        lock.writeLock().lock();
        try {
            if (this.timestamp == compressedTimestamp) {
                this.sum += transaction.getAmount();
                this.max = Double.max(this.max, transaction.getAmount());
                this.min = Double.min(this.min, transaction.getAmount());
                this.count++;
            } else {
                this.timestamp = compressedTimestamp;
                this.sum = transaction.getAmount();
                this.max = transaction.getAmount();
                this.min = transaction.getAmount();
                this.count = 1;
            }
        } finally {
            lock.writeLock().unlock();
        }
    }

    public Statistics getStatistics(Instant requestInstant, int statDepthInSeconds) {
        lock.readLock().lock();
        try {
            if ((Instant.ofEpochSecond(this.timestamp))
                    .isBefore(requestInstant.minusSeconds(statDepthInSeconds))) {
                return new Statistics(0, Double.MIN_VALUE, Double.MAX_VALUE, 0);
            } else {
                return new Statistics(
                        this.sum,
                        this.max,
                        this.min,
                        this.count);
            }
        } finally {
            lock.readLock().unlock();
        }
    }
}
