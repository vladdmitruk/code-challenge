package com.lucidcache.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;
import java.time.Instant;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TransactionControllerTest {

    @Value("${statistics.depth.in.seconds}")
    private int statisticsDepthInSeconds;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
                                                  MediaType.APPLICATION_JSON.getSubtype(),
                                                  Charset.forName("utf8"));

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void postExpiredTransaction() throws Exception {
        final String transaction = "{" +
                "\"amount\":12.3," +
                "\"timestamp\":" + Instant.now().minusSeconds(statisticsDepthInSeconds + 1).toEpochMilli() +
                "}";

        mockMvc.perform(post("/api/transactions")
                .content(transaction)
                .contentType(contentType))
                .andExpect(status().isNoContent());
    }

    @Test
    public void postUnprocessableTransaction() throws Exception {
        final String transaction = "{" +
                "\"amount\":-12.3," +
                "\"timestamp\":" + Instant.now().toEpochMilli() +
                "}";

        mockMvc.perform(post("/api/transactions")
                .content(transaction)
                .contentType(contentType))
                .andExpect(status().isUnprocessableEntity());
    }
}
