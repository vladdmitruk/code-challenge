package com.lucidcache.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;
import java.time.Instant;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StatisticsControllerTest {

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
                                                  MediaType.APPLICATION_JSON.getSubtype(),
                                                  Charset.forName("utf8"));

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setup() throws Exception {


        final String transaction1 = "{" +
                "\"amount\":12.0," +
                "\"timestamp\":" + Instant.now().toEpochMilli() +
                "}";

        final String transaction2 = "{" +
                "\"amount\":13.0," +
                "\"timestamp\":" + Instant.now().toEpochMilli() +
                "}";

        mockMvc.perform(post("/api/transactions")
                .content(transaction1)
                .contentType(contentType))
                .andExpect(status().isCreated());

        mockMvc.perform(post("/api/transactions")
                .content(transaction2)
                .contentType(contentType))
                .andExpect(status().isCreated());
    }

    @Test
    public void getStatisticsSuccessfully() throws Exception {
        mockMvc.perform(get("/api/statistics"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.sum", is(25.0)))
                .andExpect(jsonPath("$.avg", is(12.5)))
                .andExpect(jsonPath("$.max", is(13.0)))
                .andExpect(jsonPath("$.min", is(12.0)))
                .andExpect(jsonPath("$.count", is(2)));
    }
}